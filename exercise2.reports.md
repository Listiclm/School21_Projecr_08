## Ошибки в консоли на офицальном сайте СберМегаМаркета [https://sbermegamarket.ru/]
### Баг №1 - Ошибка 403 Forbidden
1. Предшествующие условия:
- открыта страница: [https://sbermegamarket.ru/]
2. Шаги воспроизведения:
- нажать F12
- открыть вкладку "Console" (Консоль) в DevTools
3. Ожидаемый результат:
- веб-приложение работает без ошибок
4. Фактический результат:
- в консоли зафиксирована следующая ошибка: POST scheme: https host: sentry-dns-customers.megamarket.tech filename: /api/6/envelope/ sentry_key: 6d82cc11d08a4657a9092ff80b4bd615 sentry_version: 7 Адрес: 37.230.196.2:443 Состояние: 403 Forbidden Версия: HTTP/1.1 ПереданоЖ 434 6 (размер 64 6) Referrer policy: strict-origin-when-cross-origin Приоритет запроса: Lowest
5. Серьезность: Minor
6. Окружение: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:109.0) Firefox/113.0

### Баг №2 - Запрос из постороннего источника заблокирован
1. Предшествующие условия:
- открыта страница: [https://sbermegamarket.ru/]
2. Шаги воспроизведения:
- нажать F12
- открыть вкладку "Console" (Консоль) в DevTools
3. Ожидаемый результат:
- веб-приложение работает без ошибок
4. Фактический результат:
- в консоли зафиксирована следующая ошибка: Запрос из постороннего источника заблокирован: Политика одного источника запрещает чтение удаленного ресурса на https://cms-res.online.sberbank.ru/sberid/BlackList/Button/No_Button.json. (Причина: не удалось выполнить запрос CORS). Код состояния: (null)
5. Серьезность: Trivial
6. Окружение: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:109.0) Firefox/113.0

### Баг №3 - TypeError, window.webim.api is undefined
1. Предшествующие условия:
- открыта страница: [https://sbermegamarket.ru/]
2. Шаги воспроизведения:
- нажать F12
- открыть вкладку "Console" (Консоль) в DevTools
3. Ожидаемый результат:
- веб-приложение работает без ошибок
4. Фактический результат:
- в консоли зафиксирована следующая ошибка: page url: https://sbermegamarket.ru/; message: watcher callback; details: TypeError, window.webim.api is undefined, resetWebimVisitor@https://extra-cdn.sbermegamarket.ru/static/dist/main.564c87f0.js:1:146542 nn@https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.89ca50aa.js:2:536746 h@https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.89ca50aa.js:2:526877 st/b.run@https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.89ca50aa.js:2:527600 er@https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.89ca50aa.js:2:542996 vn/<@https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.89ca50aa.js:2:537828 ln@https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.89ca50aa.js:2:537227
5. Серьезность: Trivial
6. Окружение: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:109.0) Firefox/113.0