## Grep
Grep – это мощная команда UNIX, позволяющая выполнять поиск внутри содержимого файла по различным параметрам.

Она особенно полезна при устранении неполадок или отладке.

Команда grep имеет огромное количество опций и вариантов использования.

Скорее всего, вам никогда не понадобятся или не пригодятся все из них.

Однако в конечном итоге вы будете использовать несколько команд grep большую часть времени.

В этой статье перечислены наиболее распространенные команды grep с краткими примерами.

Это полезно, если вы уже знакомы с командой grep, но постоянно забываете, какая опция что делает.

- grep -i pattern file - Поиск без учета регистра (Опция - i)
- grep -A n pattern file - Показать n строк после (Опция - A)
- grep -B n pattern file - 	Показать n строк перед (Опция - B)
- grep -C n pattern file - Показать n строк до и после (Опция - C)
- grep -v pattern file - Показать линии, которые не совпадают (Опция - v)
- grep -c pattern file - Подсчет количества совпадающих строк (Опция - c)
- grep -l pattern file - Отображать только имена файлов (Опция - l)
- grep -w pattern file - Совпадение точного слова (Опция - w)
- grep -e regex file - Соответствие шаблону регулярки (Опция - e)
- grep -a pattern file - Поиск в бинарных файлах (Опция - a)
- grep -r pattern dir - Рекурсивный поиск в каталоге (Опция - r)

### Быстрые примеры использования команды Grep
Вы, вероятно, уже знаете, что для поиска определенного текста или шаблона в файле нужно использовать команду grep следующим образом:
```
grep search_pattern filename
```
Рассмотрим несколько распространенных случаев использования команды grep.

### Поиск без учета регистра
По умолчанию поиск с помощью grep чувствителен к регистру.

Вы можете игнорировать соответствие регистру с помощью опции -i:
```
grep -i holmes filename
```
Таким образом, grep будет возвращать строки, соответствующие и Holmes, и holmes.

### Показать строки до и после совпадающих
По умолчанию вы видите только совпадающие строки.

Но когда вы устраняете неполадки, полезно видеть несколько строк до и/или после совпадающих строк.

Вы можете использовать параметр -A, чтобы показать строки после совпадающих.

Запомните, A – это “After” (после).

Команда, показанная ниже покажет совпадающие строки, а также 5 строк после совпадения.
```
grep -A 5 search_pattern filename
```
Аналогично, вы можете использовать параметр -B, чтобы показать строки перед совпадающими.

Запомните, что B – это “Before” ( До).

Команда, показанная ниже покажет 5 строк перед совпадающими строками вместе с совпадающей строкой (строками).
```
grep -B 5 search_pattern filename
```
Мне больше всего нравится опция -C, потому что она показывает линии, которые находятся до и после совпадающих.

Запомните, что C здесь означает Circle.

Команда ниже покажет 5 строк до совпадающей строки, совпадающую строку и 5 строк после совпадающей строки.
```
grep -C 5 search_pattern filename
```

### Показать строки, которые не совпадают
Вы можете использовать grep для отображения всех строк, которые НЕ соответствуют заданному шаблону.

Это “инвертированное соответствие” используется с опцией -v:
```
grep -v search_pattern filename
```
Вы можете комбинировать опции -i и -v.

### Подсчет количества совпадающих строк
Вместо того чтобы показывать совпадающие строки, вы можете просто узнать, сколько строк соответствует шаблону с помощью опции -c.

Это строчная буква c.
```
grep -c search_pattern filename
```
Вы можете комбинировать опции -c и -v, чтобы получить количество строк, не соответствующих заданному шаблону.  
Конечно, вы можете использовать опцию -i, не чувствительную к регистру.

### Показать номера совпадающих строк
Чтобы показать номера совпадающих строк, вы можете использовать опцию -n.
```
grep -n search_pattern filename
```
То же самое можно сделать с инвертированным поиском.

### Поиск в нескольких файлах
Вы можете предоставить grep несколько файлов для поиска.
```
grep search_pattern file1 file2
```
Это может сработать, но более практичным примером является поиск в файлах определенного типа.

Например, если вы хотите искать строку только в скриптах оболочки (файлы, заканчивающиеся на .sh), вы можете использовать:
```
grep search_pattern *.sh
```
### Рекурсивный поиск всех файлов в каталоге
Вы можете выполнить рекурсивный поиск с помощью опции grep -r.

Она будет искать заданный шаблон во всех файлах в текущем каталоге и его подкаталогах.
```
grep -r search_pattern directory_path
```
### Отображать только имена файлов
По умолчанию grep показывает все совпадающие строки.

Если вы запустили поиск в нескольких файлах и хотите увидеть только те файлы, которые содержат строку, вы можете использовать опцию -l.
```
grep -l search_pattern files_pattern
```
Скажем, вы хотите посмотреть, какие файлы Markdown содержат слово “handbook”, вы можете использовать:
```
grep -l handbook *.md
```
### Поиск только полного слова
По умолчанию grep показывает все строки, содержащие заданную строку.

Это не всегда нужно.

Если вы ищете слово ‘done’, он также покажет строки, содержащие слова ‘doner’ или ‘abandoned’.

Чтобы заставить grep искать только полное слово, вы можете использовать опцию -w:
```
grep -w search_string file
```
Таким образом, при поиске слова “done” будут показаны только строки, содержащие “done”, а не “doner” или “abandoned”.

### Поиск по шаблонам регулярных выражений
Вы можете расширить возможности поиска, используя шаблоны regex.

Существует специальная опция -e, позволяющая использовать шаблоны regex, и опция -E, позволяющая использовать расширенные шаблоны regex.
```
grep -e regex_pattern file
```
### Поиск того или иного шаблона
Вы можете искать несколько шаблонов в одном поиске grep.

Если вы хотите увидеть строки, содержащие тот или иной паттерн, вы можете использовать оператор |.

Однако этот специальный символ необходимо экранировать следующим образом.
```
grep 'pattern1\|pattern' filename
```
С оператором OR можно использовать несколько шаблонов.

Для оператора AND нет специальной опции.

Для этого можно использовать grep несколько раз с перенаправлением пайпа.
### Поиск в бинарниках
По умолчанию Grep игнорирует бинарные файлы.

Вы можете заставить его выполнять, как в текстовых, используя опцию -a.
```
grep -a pattern binary_file
```

## Diff
Утилита Diff Linux - это программа, которая работает в консольно режиме. Ее синтаксис очень прост. Вызовите утилиту, передайте нужные файлы, а также задайте опци, если это необходимо.

### $ diff опции файл1 файл2

Можно передать больше двух файлов, если это нужно. Перед тем как перейти к примерам, давайте рассмотрим опции утилиты:

- -q — выводить только отличия файлов;
- -s — выводить только совпадающие части;
- -с — выводить нужное количество строк после совпадений;
- -u — выводить только нужное количество строк после отличий;
- -y — выводить в две колонки;
- -e — вывод в формате ed скрипта;
- -n — вывод в формате RCS;
- -a — сравнивать файлы как текстовые, даже если они не текстовые;
- -t — заменить табуляции на пробелы в выводе;
- -l — разделить на страницы и добавить поддержку листания;
- -r — рекурсивное сравнение папок;
- -i — игнорировать регистр;
- -E — игнорировать изменения в табуляциях;
- -Z — не учитывать пробелы в конце строки;
- -b — не учитывать пробелы;
- -B — не учитывать пустые строки.

Это были основные опции утилиты, теперь давайте рассмотрим как сравнить файлы Linux. В выводе утилиты кроме, непосредственно, отображения изменений, выводит строку в которой указывается в какой строчке и что было сделано. Для этого используются такие символы:

- a — добавлена;
- d — удалена;
- c — изменена.

К тому же, линии, которые отличаются, будут обозначаться символом <, а те, которые совпадают — символом >.

Примеры:

1. Сравнение файлов Diff:
```
$ diff file1 file2
```
2. Использование опции для игнорирования регистра:
```
$ diff -i file1 file2
```
3. Можно сделать вывод в две колонки:
```
$ diff -y file1 file2
```
4. C помощью опции -u вы можете создать патч, который потом может быть наложен на такой же файл другим пользователем:
```
$ diff -u file1 file2
```
5. Чтобы обработать несколько файлов в папке удобно использовать опцию -r:
```
$ diff -r ~/tmp1 ~/tmp2
```
6. Для удобства, вы можете перенаправить вывод утилиты сразу в файл:
```
$ diff -u file1 file2 > file.patch
```