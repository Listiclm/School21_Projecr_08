## Уровни и типы логов  

Лог — это текстовый файл, который содержит системную информацию о работе ПО или сервера. В лог вносятся данные о действиях, выполненных программой или пользователем.

Логирование необходимо на всех этапах разработки, а также во время дальнейшей эксплуатации ПО. Так может накопиться большое количество лог-файлов, в которых будет очень сложно разобраться. Поэтому для удобства и упрощения поиска и чтения логов, их делят на уровни и типы.

### Выделяют четыре основных уровня логов:

- Debug — запись масштабных переходов состояний: обращения к базам данных, запуск и остановка сервиса;
- Warning — внештатные ситуации, например, неправильный формат запроса;
- Error — запись типичных ошибок;
- Fatal — фатальные сбои в работе: отказ доступа к базе данных, нехватка места на диске.

Есть еще два дополнительных уровня логирования:

- Trace — запись процесса по шагам; нужен, когда трудно локализовать проблему;
- Info — общая информация о работе сервиса, службы.

Типы логов:  
- Серверные — обращения к серверу и ошибки, которые возникают во время обращений;
- Системные — все системные события;
- Логи авторизации и аутентификации — процессы входа в систему и выхода из нее, проблемы с доступом и другие;
- Логи приложений, которые находятся в этой системе;
- Логи баз данных — обращения к БД.

## Файл с логами имеет расширение .Log

### Что может содержаться в логах

В лог-файлах находится полный журнал событий, связанных с конкретным узлом. Там описываются время события, тип запроса, реакция сервера, код ответа, IP-адрес пользователя, количество переданной информации и многое другое. Если произошла ошибка, это будет помечено в логах отдельно.

Но вся перечисленная информация представлена в очень сжатом виде. Поэтому незнакомый с правилами записи человек может в ней запутаться. Более того, в логах много сведений, поэтому они очень подробные и обширные. Бывает сложно отделить нужную информацию от той, которая не пригодится сейчас.

### Как правильно читать лог

### Вручную. 
Логи хранятся в файлах с расширением .log. Их можно открыть как обычные текстовые файлы и просмотреть содержимое. Перед этим стоит посмотреть, как настроен формат записи логов, если у вас есть доступ к этим параметрам.

Например, так выглядит формат по умолчанию для лога доступа с веб-сервера:

[доменное имя сайта][IP-адрес пользователя][дата и время визита][тип запроса][URL, к которому обратился пользователь][протокол, по которому пользователь соединился с сайтом][код ответа сервера][количество байт информации, которую передали пользователю][дополнительная информация]

Данные чаще всего разделяются пробелами, иногда также дефисами или слэшами. Каждая запись показывается с новой строчки. Читать полные логи в таком формате довольно трудоемко, поэтому главное — найти нужные строки и сконцентрироваться на них.

Это не единственный возможный способ записи лога. Например, в логе ошибок каждая строчка — это запись об ошибке с полной информацией о ней: датой и временем, адресом страницы, на которой возник сбой, и так далее.

### С помощью анализатора. 

Второй вариант — не просматривать лог вручную, а воспользоваться специальной программой-анализатором. Она парсит лог-файл — «разбирает» его на составляющие и представляет в удобном для пользователя виде. Так информация показывается в виде понятного отчета, иногда с графиками и диаграммами.

Анализаторы бывают разными, например Weblog Expert, Analog и пр. Некоторые из них также умеют интегрироваться с сервисами для сбора статистики, чтобы показывать более полную картинку.

Проверять и читать логи вам понадобится, если вы будете работать с профессиональным ПО для разработчиков, вебмастеров или инженеров. Это сложно только с первого взгляда — если понять принцип, расшифровать их не составит труда. А анализаторы помогут лучше и быстрее сориентироваться в записях.

##  Для просмотра логов используют следующие программы

- Блокнот и ему подобные текстовые редакторы
- Notepad2
- Microsoft Notepad
- Microsoft Word
- Microsoft WordPad
- Microsoft Works
- Apple console
- Apple Text Edit
- OpenOffice Writer
- LogViewer Pro
- GroupDocs Viewer
- и т.д.
